package dnn.dao.user;

import java.util.List;

import dnn.dao.CrudRepository;
import dnn.entity.User;

public interface UserRepository extends CrudRepository<User, String>{
	
	Long countByLastname(String lastname);
	
	Long deleteByLastname(String lastname);
	
	List<User> removeByLastname(String lastname);
	
	List<User> findByLastname(String lastname);

}
