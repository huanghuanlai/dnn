package dnn.web.admin.clouddisk;

import com.dounine.clouddisk360.parser.deserializer.login.LoginUserToken;
import com.dounine.clouddisk360.pool.PoolingHttpClientConnection;
import com.dounine.clouddisk360.store.BasePathCommon;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("admin/clouddisk")
public class ClouddiskAction {

	public static final LoginUserToken LOGIN_USER_TOKEN =
			new LoginUserToken("360云盘帐号",
			"不经md5加密密码",false);

	static {
		BasePathCommon.BASE_PATH = "/Users/huanghuanlai/Desktop/clouddisk/";
		PoolingHttpClientConnection.initHttpClientConnectionManager();
	}

}
